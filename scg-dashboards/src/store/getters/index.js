export const getGoodList = (state) => {
    return state.shopModule.all;
}

export const getProfile = (state) => {
    return state.profileModule.profile;
}

export const getGoodById = (state) => (id) => {
    return state.shopModule.all.find((data) => data['_id'] === id);
}

export const getDetailDataList = (state) => {
    return state.shopModule.detail;
}

export const getHistoryList = (state) => {
    return state.shopModule.history;
}