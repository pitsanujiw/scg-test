import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import shopModule from './modules/shopModule'
import profileModule from './modules/profileModule'


Vue.use(Vuex)

const strict = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    actions,
    getters,
    modules: {
        shopModule,
        profileModule,
    },
    strict
})