import {
    getGoodDataList,
    getProfile,
    updateProfile,
    inserGood,
    updateGood,
    deleteByIdList,
    detailData,
    historyBought
} from '@/services'

export const getShopData = async ({
    commit
}) => {
    try {
        const data = await getGoodDataList()
        commit('RECEIVE_PRODUCTS', data)
        return true;
    } catch (error) {
        return false;
    }
}

export const getDetailData = async ({
    commit
}) => {
    try {
        const data = await detailData()
        commit('RECEIVE_DETAIL', data)
        return true;
    } catch (error) {
        return false;
    }
}

export const getHistoryData = async ({
    commit
}) => {
    try {
        const data = await historyBought()
        commit('RECEIVE_HISTORY', data)
        return true;
    } catch (error) {
        return false;
    }
}


export const getProfileData = async ({
    commit
}) => {
    try {
        const data = await getProfile()
        commit('RECEIVE_PROFILE', data)
        return true;
    } catch (error) {
        return false;
    }
}

export const updateProfileAction = async (_, data) => {
    try {
        const result = await updateProfile(data);
        if (result)
            return true;
        return false;
    } catch (error) {
        return false;
    }
}

export const deleteByIdListAction = async (_, data) => {
    try {
        const result = await deleteByIdList(data);
        if (result)
            return true;
        return false;
    } catch (error) {
        return false;
    }
}

export const insertGoodAction = async (_, data) => {
    try {
        const result = await inserGood(data);
        if (result)
            return true;
        return false;
    } catch (error) {
        return false;
    }
}

export const updateGoodAction = async (_, data) => {
    try {
        const result = await updateGood(data);
        console.log(result)
        if (result)
            return true;
        return false;
    } catch (error) {
        return false;
    }
}