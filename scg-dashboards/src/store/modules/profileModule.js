const state = {
    profile: {}
}

const mutations = {
    RECEIVE_PROFILE(state, profile) {
        state.profile = profile
    }
}

export default {
    state,
    mutations
}