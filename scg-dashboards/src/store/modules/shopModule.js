const state = {
    all: [],
    detail: {},
    history: []
}

const mutations = {
    RECEIVE_PRODUCTS(state, products) {
        state.all = products
    },
    RECEIVE_DETAIL(state, detaildata) {
        state.detail = detaildata;
    },
    RECEIVE_HISTORY(state, his) {
        state.history = his;
    }
}

export default {
    state,
    mutations
}