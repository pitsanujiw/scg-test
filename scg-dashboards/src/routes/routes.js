import Vue from 'vue'
import VueRouter from 'vue-router'
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Notifications from "@/pages/Notifications.vue";

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: configRoutes(),
  linkExactActiveClass: "nav-item active"
})

function configRoutes() {
  return [{
      path: "/",
      redirect: "/dashboard",
      component: () => import('@/pages/Layout/DashboardLayout.vue'),
      children: [{
          path: "dashboard",
          name: "Dashboard",
          meta: {
            requiresAuth: true
          },
          component: () => import('@/pages/Dashboard.vue')
        },
        {
          path: "user",
          name: "User Profile",
          meta: {
            requiresAuth: true
          },
          component: () => import('@/pages/UserProfile.vue')
        },
        {
          path: "table",
          name: "Table List",
          meta: {
            requiresAuth: true
          },
          component: () => import('@/pages/TableList.vue')
        },
        {
          path: "typography",
          name: "Typography",
          component: Typography,
          meta: {
            requiresAuth: true
          },
        },
        {
          path: "icons",
          name: "Icons",
          component: Icons,
          meta: {
            requiresAuth: true
          },
        },
        {
          path: "notifications",
          name: "Notifications",
          component: Notifications,
          meta: {
            requiresAuth: true
          },
        },
        {
          path: "good/edit/:id",
          name: "good Edit",
          component: () => import('@/pages/EditGood.vue'),
          meta: {
            requiresAuth: true
          },
        },
        {
          path: "good/create",
          name: "good create",
          component: () => import('@/pages/CreateGood.vue'),
          meta: {
            requiresAuth: true
          },
        },
      ]
    },
    {
      path: "/login",
      name: "login",
      component: () => import('@/pages/Login.vue'),
    }
  ];
}

router.beforeEach((to, from, next) => {
  if (to?.matched?.some(route => route.meta.requiresAuth)) {
    if (localStorage.getItem('auth')) {
      next()
    } else {
      next({
        path: 'login',
        replace: true
      })
    }
  } else {
    next()
  }
})



export default router;