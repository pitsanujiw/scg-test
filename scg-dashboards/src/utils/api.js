import axios from 'axios'
const API_ROOT = 'http://localhost:3000'
const TIMEOUT = 60000
const HEADERS = {
  'Content-Type': 'application/json'
}
export class ApiService {
  constructor({
    baseURL = API_ROOT,
    timeout = TIMEOUT,
    headers = HEADERS
  }) {
    const client = axios.create({
      baseURL,
      timeout,
      headers
    })
    this.client = client
  }

  async get(path, option = {}) {
    return this.client.get(path, option).then((response) => response.data)
  }

  async post(path, payload, option = {}) {
    return this.client.post(path, payload, option).then((response) => response.data)
  }

  async put(path, payload) {
    return this.client.put(path, payload).then((response) => response.data)
  }

  async patch(path, payload) {
    return this.client.patch(path, payload).then((response) => response.data)
  }

  async delete(path) {
    return this.client.delete(path).then((response) => response.data)
  }
}