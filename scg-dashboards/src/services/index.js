import {
    ApiService
} from '@/utils'

const client = new ApiService({})

export const getGoodDataList = async () => {
    return await client.get('/api/v1/shop-data')
}
export const getProfile = async () => {
    const data = await client.post('/api/v1/user-info', '', {
        headers: {
            'username': 'Example'
        }
    })
    return data;
}

export const updateProfile = async (data) => {
    return await client.post('/api/v1/user-info/update', data, {
        headers: {
            'username': 'Example'
        }
    })
}

export const inserGood = async (data) => {
    return await client.post('/api/v1/shop-data/insert-goods', data, {
        headers: {
            'username': 'Example'
        }
    })
}

export const updateGood = async (data) => {
    return await client.put('/api/v1/shop-data/update-goods', data, {
        headers: {
            'username': 'Example'
        }
    })
}

export const getById = async (id) => {
    return await client.get(`api/v1/shop-data/${id}`, {
        headers: {
            'username': 'Example'
        }
    })
}

export const deleteByIdList = async (data) => {
    return await client.post(`api/v1/shop-data/delete`, data, {
        headers: {
            'username': 'Example'
        }
    })
}

export const detailData = async () => {
    return await client.get(`/api/v1/shop-data/detail-shop`, {
        headers: {
            'username': 'Example'
        }
    })
}

export const historyBought = async () => {
    return await client.get(`/api/v1/history`, {
        headers: {
            'username': 'Example'
        }
    })
}