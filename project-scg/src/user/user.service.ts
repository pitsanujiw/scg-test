import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UsersDocument, Users } from './schema/users.schema';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(Users.name) private readonly usersModel: Model<UsersDocument>,
    ) { }

    async findMyUserName(username: string) {
        try {
            return await this.usersModel.findOne({ 'username': username });
        } catch (error) {
            throw error;
        }
    }

    async reduceCredit(name: string, limit: number) {
        try {
           return await this.usersModel.updateOne({
                username: name
            }, {
                $inc: { credit: -limit }
            });
        } catch (error) {
            throw error;
        }
    }

    async updateProfile(profile: Users) {
        try {
            return await this.usersModel.updateOne({ _id: profile['_id'] }, { $set: profile });
        } catch (error) {
            throw error;
        }
    }
}
