import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UsersDocument = Users & Document;

@Schema({ timestamps: true })
export class Users {
    @Prop()
    username: string;

    @Prop()
    credit: number;

    @Prop()
    firstName: string;

    @Prop()
    lastName: string;

    @Prop()
    email: string;
}

export const UsersSchema = SchemaFactory.createForClass(Users);
