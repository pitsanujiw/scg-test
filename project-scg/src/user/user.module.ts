import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Users, UsersSchema } from './schema/users.schema';
@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Users.name, schema: UsersSchema
      }
    ])
  ],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
