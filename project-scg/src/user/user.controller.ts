import { Users } from './schema/users.schema';
import { Controller, Post, Req, Request } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user-info')
export class UserController {
    constructor(private readonly userInfoService: UserService) {

    }
    @Post()
    public async getUserInfo(@Req() request: Request) {
        try {
            const username: string = request.headers['username'] as unknown as string;
            if (username) return await this.userInfoService.findMyUserName(username);
            throw null;
        } catch (error) {
            throw error;
        }
    }

    @Post('reduceCredit')
    public async reduceCredit(@Req() request: Request) {
        try {
            const username: string = request.headers['username'] as unknown as string;
            const credit: number = (request.body as any).credit as number;
            if (username && credit) return await this.userInfoService.reduceCredit(username, credit);
        } catch (error) {
            throw error;
        }
    }

    @Post('update')
    public async updateProfile(@Req() request: Request) {
        try {
            const username: string = request.headers['username'] as unknown as string;
            const data: Users = request.body as unknown as Users;
            if (username && data) return await this.userInfoService.updateProfile(data);
        } catch (error) {
            throw error;
        }
    }
}
