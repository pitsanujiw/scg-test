import { History, HistoryDocument } from './schema/historyCart.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';


@Injectable()
export class HistoryService {
    constructor(
        @InjectModel(History.name) private readonly historyCartModel: Model<HistoryDocument>,
    ) { }

    async getAll() {
        return await this.historyCartModel.find().exec();
    }

    async insertLogHistory(data: any) {
        try {
            return await this.historyCartModel.create(data);
        } catch (error) {
            throw error;
        }
    }

    async sumRevenue() {
        try {
            const data = await this.getAll();
            const revenueCount = data.reduce((a, b) => a + b.total, 0);
            return {
                title: 'Revenue',
                count: revenueCount
            }
        } catch (error) {
            throw error;

        }
    }
}
