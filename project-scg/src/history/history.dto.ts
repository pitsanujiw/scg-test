export interface BuyList {
    title: string;
    quantity: number;
}