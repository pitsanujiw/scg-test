import { HistoryService } from './history.service';
import { Controller, Delete, Get, Param, Post, Put, Req, Request } from '@nestjs/common';

@Controller('history')
export class HistoryController {
    constructor(private readonly historyService: HistoryService) { }

    @Get()
    public async getHistory() {
        try {
            return await this.historyService.getAll();
        } catch (error) {
            return error
        }
    }

    @Post('insert')
    public async insertHistoryClient(@Req() request: Request) {
        try {
            const data = (request.body as any) as unknown as any[];

            return await this.historyService.insertLogHistory(data);
        } catch (error) {
            return error
        }
    }
}
