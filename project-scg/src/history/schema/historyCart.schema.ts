import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { BuyList } from '../history.dto';

export type HistoryDocument = History & Document;

@Schema({ timestamps: true })
export class History {
    @Prop()
    username: string;

    @Prop()
    buyList: BuyList[];

    @Prop()
    total: number;
}

export const HistorySchema = SchemaFactory.createForClass(History);
