import * as nodemailer from 'nodemailer';
import { Goods } from '../shop-data/schema/shop-data.schema';

// async..await is not allowed in global scope, must use a wrapper
export const sendEmail = async (email: string, data: Goods) => {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: 'smtp.sendgrid.net',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: 'apikey',
            pass: process.env.SENDGRID_API_KEY,
        },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
        from: '"admin@scg-backoffice" <admin@scg-no-reply.com>', // sender address
        to: process.env.EMAILADMIN, 
        subject: `${data.title} Almost run out Good`, // Subject line
        text: `${data.title} Almost run out Good, Please check about Good`, // plain text body
        html: `<b>${data.title} Almost run out Good ${data.inventory} count, Please check about Good</b>`, // html body
    });

    console.log('Message sent: %s', info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
};