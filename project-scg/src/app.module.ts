import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ShopDataModule } from './shop-data/shop-data.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { HistoryModule } from './history/history.module';

@Module({
  imports: [ShopDataModule,
    MongooseModule.forRoot(process.env.mongoDB, { useNewUrlParser: true }),
    UserModule,
    HistoryModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
