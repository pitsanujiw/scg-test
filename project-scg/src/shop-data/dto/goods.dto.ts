export class GoodsDto {
    readonly title: string;
    readonly price: number;
    readonly inventory: number;
    readonly shipping: number;
}