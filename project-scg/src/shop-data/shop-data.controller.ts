import { HistoryService } from './../history/history.service';
import { Controller, Delete, Get, Param, Post, Put, Req, Request } from '@nestjs/common';
import { Goods } from './schema/shop-data.schema';
import { ShopDataService } from './shop-data.service';

@Controller('shop-data')
export class ShopDataController {
    constructor(private readonly shopDataService: ShopDataService,
        private readonly historyService: HistoryService) { }
    @Get()
    public async getShopData() {
        return await this.shopDataService.findAll();
    }

    @Get('detail-shop')
    public async getDetail() {
        try {
            const subTotal = await this.shopDataService.sumTotalGood();
            const sumRevenue = await this.historyService.sumRevenue();
            return {
                subTotal,
                sumRevenue
            }
        } catch (error) {
            return error;
        }
    }

    @Get(':id')
    public async getById(@Param() params) {
        try {
            return await this.shopDataService.findById(params.id);
        } catch (error) {
            return error;
        }
    }

    @Post('delete')
    public async deleteById(@Req() request: Request) {
        const data = (request.body as any).idList as unknown as string[];
        try {
            return await this.shopDataService.deleteByIdList(data);
        } catch (error) {
            return error;
        }
    }

    @Post('buyer')
    public async updateInventroyGoods(@Req() request: Request) {
        const data = request.body as unknown as Goods[];
        try {
            if (data) return await this.shopDataService.updateInventory(data);
            return null;
        } catch (error) {
            return error;
        }
    }

    @Post('insert-goods')
    public async insertGood(@Req() request: Request) {
        try {
            const data = request.body as unknown as Goods;
            if (data) return await this.shopDataService.insertGood(data);
            return null
        } catch (error) {
            return error;
        }
    }

    @Put('update-goods')
    public async updateGood(@Req() request: Request) {
        try {
            const data = request.body as unknown as Goods;
            if (data) return await this.shopDataService.updateGood(data);
            return null
        } catch (error) {
            return error;
        }
    }

}
