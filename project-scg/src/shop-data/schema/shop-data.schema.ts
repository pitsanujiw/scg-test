import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type GoodsDocument = Goods & Document;

@Schema({ timestamps: true })
export class Goods {
    @Prop()
    title: string;

    @Prop()
    price: number;

    @Prop()
    inventory?: number;

    @Prop()
    shipping?: number;
}

export const GoodsSchema = SchemaFactory.createForClass(Goods);