import { HistoryModule } from './../history/history.module';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ShopDataController } from './shop-data.controller';
import { ShopDataService } from './shop-data.service';
import { Goods, GoodsSchema } from './schema/shop-data.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Goods.name, schema: GoodsSchema
      }
    ]),
    HistoryModule,
  ],
  controllers: [ShopDataController],
  providers: [ShopDataService]
})
export class ShopDataModule { }
