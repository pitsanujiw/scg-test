import { sendEmail } from '../utils/sendEmail';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GoodsDocument, Goods } from './schema/shop-data.schema';
const emailAdmin = process.env.EMAILADMIN;

@Injectable()
export class ShopDataService {
    constructor(
        @InjectModel(Goods.name) private readonly goodModel: Model<GoodsDocument>,
    ) { }

    async findAll(): Promise<Goods[]> {
        return this.goodModel.find().sort('-1').exec();
    }

    async findById(id: string) {
        return this.goodModel.findById(id).exec();
    }

    async updateInventory(goodsList: Goods[]) {
        try {
            const updates = [];
            const sendEmailList = [];
            goodsList.forEach(async (data) => {
                const updatePromise = this.goodModel.updateOne({ _id: data['_id'] }, { $set: data });
                updates.push(updatePromise);
                if (data.inventory < 10) {
                    sendEmailList.push(sendEmail(emailAdmin, data));
                }
            })
            // supported it's have api key and hosting
            if (sendEmailList.length > 0) await Promise.all(sendEmailList);
            const result = await Promise.all(updates);
            return result;
        } catch (error) {
            throw error;
        }
    }

    async deleteByIdList(idList: string[]) {
        try {
            const deleteList = [];
            idList.forEach((id) => {
                const promiseDel = this.goodModel.deleteOne({ _id: id }).exec();
                deleteList.push(promiseDel);
            })
            const result = await Promise.all(deleteList);
            return result;
        } catch (error) {
            throw error;
        }
    }


    async sumTotalGood() {
        try {
            const data = await this.findAll();
            const inventory = data.reduce((a: number, b) => a + b.inventory, 0);
            return {
                title: 'Inventory',
                count: inventory
            }
        } catch (error) {
            throw error;

        }
    }

    async insertGood(good: Goods) {
        try {
            return await this.goodModel.create(good);
        } catch (error) {
            throw error;
        }
    }

    async updateGood(good: Goods) {
        try {
            return await this.goodModel.updateOne({ _id: good['_id'] }, { $set: good });
        } catch (error) {
            throw error;
        }
    }
}
